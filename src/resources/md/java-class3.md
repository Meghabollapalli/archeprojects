# Collections Framework

The Collections Framework in Java provides a set of interfaces and classes that are used to store and manipulate collections of objects. It provides a unified architecture for representing and manipulating collections, allowing them to be easily organized, accessed, and modified. Here are some important components of the Collections Framework:

![Alt text](https://data-flair.training/blogs/wp-content/uploads/sites/2/2018/03/hierarchy-of-collection-framework-in-java.webp "a title")


## 1. Interfaces

### a. List

- Ordered collection that allows duplicate elements.
- Common implementations include ArrayList and LinkedList.

```java
List<String> names = new ArrayList<>();
names.add("Alice");
names.add("Bob");
names.add("Alice");

System.out.println(names); // Output: [Alice, Bob, Alice]
```

### b. Set

- Collection that does not allow duplicate elements.
- Common implementations include HashSet and TreeSet.

```java
Set<Integer> numbers = new HashSet<>();
numbers.add(1);
numbers.add(2);
numbers.add(1);

System.out.println(numbers); // Output: [1, 2]
```

### c. Map

- Collection that stores key-value pairs.
- Keys are unique, and each key is associated with a value.
- Common implementations include HashMap and TreeMap.

```java
Map<String, Integer> ages = new HashMap<>();
ages.put("Alice", 25);
ages.put("Bob", 30);
ages.put("Alice", 27);

System.out.println(ages.get("Alice")); // Output: 27
```

### d. Queue

- Collection that follows the First-In-First-Out (FIFO) order.
- Common implementations include LinkedList and PriorityQueue.

```java
Queue<String> queue = new LinkedList<>();
queue.offer("A");
queue.offer("B");
queue.offer("C");

System.out.println(queue.poll()); // Output: A
```

### e. Deque

- Collection that allows insertion and removal from both ends.
- Common implementations include ArrayDeque and LinkedList.

```java
Deque<String> deque = new ArrayDeque<>();
deque.offerFirst("A");
deque.offerLast("B");
deque.offerFirst("C");

System.out.println(deque.pollLast()); // Output: B
```

### f. SortedSet

- Collection that stores unique elements in sorted order.
- Common implementations include TreeSet.

```java
SortedSet<Integer> numbers = new TreeSet<>();
numbers.add(3);
numbers.add(1);
numbers.add(2);

System.out.println(numbers.first()); // Output: 1
```

### g. SortedMap

- Collection that stores key-value pairs in sorted order of keys.
- Common implementations include TreeMap.

```java
SortedMap<String, Integer> ages = new TreeMap<>();
ages.put("Alice", 25);
ages.put("Bob", 30);
ages.put("Charlie", 35);

System.out.println(ages.lastKey()); // Output: Charlie
```

## 2. Classes

### a. ArrayList

- Implements the List interface using a dynamic array.
- Provides fast random access but slower insertions and removals.

```java
List<String> colors = new ArrayList<>();
colors.add("Red");
colors.add("Green");
colors.add("Blue");

System.out.println(colors.get(1)); // Output: Green
```

### b. LinkedList

- Implements the List and Deque interfaces using a doubly-linked list.
- Provides fast insertions and removals but slower random access.

```java
List<String> names = new LinkedList<>();
names.add("Alice");
names.add("Bob");
names.add("Charlie");

System.out.println(names.removeLast()); // Output: Charlie
```

### c. HashSet

- Implements the Set interface using a hash table.
- Provides constant-time performance for basic operations.

```java
Set<String> fruits = new HashSet<>();
fruits.add("Apple");
fruits.add("Banana");
fruits.add("Orange");

System.out.println(fruits.contains("Banana")); // Output: true
```

### d. HashMap

- Implements the Map interface using a hash table.
- Provides fast retrieval and update of key-value pairs.

```java
Map<String, Integer> ages = new HashMap<>();
ages.put("Alice", 25);
ages.put("Bob", 30);
ages.put("Charlie", 35);

System.out.println(ages.get("Bob")); // Output: 30
```

### e. PriorityQueue

- Implements the Queue interface using a priority heap.
- Elements are ordered based on their priority.

```java
Queue<Integer> priorityQueue = new PriorityQueue<>();
priorityQueue.offer(3);
priorityQueue.offer(1);
priorityQueue.offer(2);

System.out.println(priorityQueue.poll()); // Output: 1
```

### f. LinkedHashMap

- Implements the Map interface using a hash table with a linked list for iteration order.
- Maintains the insertion order of elements.

```java
Map<String, Integer> scores = new LinkedHashMap<>();
scores.put("Alice", 85);
scores.put("Bob", 92);
scores.put("Charlie", 78);

System.out.println(scores.keySet()); // Output: [Alice, Bob, Charlie]
```

### g. EnumSet

- Specialized Set implementation for enum types.
- Provides efficient memory usage and fast set operations.

```java
Set<DayOfWeek> weekendDays = EnumSet.of(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);
Set<DayOfWeek> allDays = EnumSet.allOf(DayOfWeek.class);

System.out.println(weekendDays); // Output: [SATURDAY, SUNDAY]
```

### h. ConcurrentHashMap

- Implements the Map interface using a hash table.
- Provides thread-safe operations without the need for external synchronization.

```java
Map<String, Integer> concurrentMap = new ConcurrentHashMap<>();
concurrentMap.put("Alice", 25);
concurrentMap.put("Bob", 30);
concurrentMap.put("Charlie", 35);

System.out.println(concurrentMap.get("Bob")); // Output: 30
```
