package class2;

abstract class VehicleAbstract {

  void engine(){
    System.out.println("from engine method inside abstract");
  }

  void engine1() {

  }

  /*
  abstract class -> grouping -> Animal-> mamal(), makeNoise(), eat(), sleep(), Cat, dog, snake, rat, rabbit
  interface -> segregation -> banking -> savings, current, trading -> currentBalance(), intrerestRate()
   */
}
