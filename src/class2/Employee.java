package class2;

public class Employee {

  String name;
  int age;

  Employee(){};

  Employee(String name, int age){
    this.name = name;
    this.age = age;
  }

  //setters
  public void setAge(int age){
    this.age = age;
  }

  public void setName(String name){
    this.name = name;

  }

  //getters
  public String getName(){
    return this.name;
  }

  public int getAge(){
    return this.age;
  }

  public static void main(String[] args) {

    Employee e = new Employee("Ravi", 20);
    Employee e1 = new Employee();
    e1.setAge(24); //doesn't return anything
    e1.setName("rishi");
    e1.getAge();
    System.out.println(e1.getAge());
    System.out.println(e1.getName());
  }
}
