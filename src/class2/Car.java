package class2;

public class Car extends Vehicle implements VehicleInterface, SecondInterface{

  @Override
  void engine(){
    System.out.println("its cars engine");
  }

  public static void main(String[] args) {
    Vehicle v1 = new Car();
    v1.engine();
    Car c = new Car();
    System.out.println(c.calcuateCC(100, 3));
  }

  public int calcuateCC(int CC){
    return CC * 2;
  }

  public int calcuateCC(int CC, int multiplier){
    return CC * multiplier;
  }


  @Override
  public void print() {
    System.out.println("printing something");
  }

  @Override
  public void draw() {
    System.out.println("draw something");
  }
}
