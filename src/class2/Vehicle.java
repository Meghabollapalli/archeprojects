package class2;

public class Vehicle {

  void engine(){
    System.out.println("the capacity of the engine");
  }

  void engine1(){
    System.out.println("this is from engine1 method");
  }

 /*
 abstarct classes -> can have abstract and non abstarct methods
 interfaces -> can have only abstract methods
 normal classes -> we will always have implementation/non-abstract methods
  */
}
