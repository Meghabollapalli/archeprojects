## Object-Oriented Programming (OOP)

Object-Oriented Programming (OOP) is a programming paradigm that focuses on the organization of code around objects, which are instances of classes. OOP offers several key concepts and principles that enhance code reusability, modularity, and maintainability. Let's explore each concept with examples:

1. **Classes and Objects:**
   - **Classes:** A class is a blueprint or template that defines the properties (attributes) and behaviors (methods) of objects. It serves as a blueprint for creating multiple objects with similar characteristics.

   Example:
   ```java
   class Person {
       String name;
       int age;
   }
   ```

   - **Objects:** Objects are instances of classes. They encapsulate data and behavior, allowing manipulation and interaction with the data through methods.

   Example:
   ```java
   Person person1 = new Person();
   person1.name = "John";
   person1.age = 30;
   ```

2. **Encapsulation:**
   - Encapsulation is the process of bundling data and methods within a class, hiding the internal details and providing access through well-defined interfaces.

   Example:
   ```java
   class Person {
       private String name;
       private int age;

       public String getName() {
           return name;
       }

       public void setName(String name) {
           this.name = name;
       }

       public int getAge() {
           return age;
       }

       public void setAge(int age) {
           this.age = age;
       }
   }
   ```

3. **Inheritance:**
   - Inheritance allows the creation of new classes (derived or child classes) based on existing classes (base or parent classes). It enables code reuse and promotes the concept of a hierarchical classification of classes.

   Example:
   ```java
   class Vehicle {
       String brand;
       void start() {
           System.out.println("The vehicle starts.");
       }
   }

   class Car extends Vehicle {
       int numWheels;
   }
   ```

4. **Polymorphism:**
   - Polymorphism refers to the ability of objects of different classes to be treated as objects of a common superclass. It allows flexibility and dynamic behavior based on the actual type of the object at runtime.

   Example:
   ```java
   class Animal {
       void makeSound() {
           System.out.println("The animal makes a sound.");
       }
   }

   class Dog extends Animal {
       void makeSound() {
           System.out.println("The dog barks.");
       }
   }

   class Cat extends Animal {
       void makeSound() {
           System.out.println("The cat meows.");
       }
   }
   ```

   Example of Polymorphism:
   ```java
   Animal animal1 = new Dog();
   Animal animal2 = new Cat();

   animal1.makeSound(); // Output: The dog barks.
   animal2.makeSound(); // Output: The cat meows.
   ```

5. **Abstraction:**
   - Abstraction focuses on representing essential features without exposing the underlying complexity. It simplifies the interaction between objects by providing a clear and concise interface.

   Example:
   ```java
   abstract class Shape {
       abstract void draw();
   }

   class Circle extends Shape {
       void draw() {
           System.out.println("Drawing a circle.");
       }
   }
   ```

6. **Association, Aggregation, and Composition:**
   - **Association:** Association represents a relationship between two or more objects, where each object maintains its independent lifecycle.

   Example:
   ```java
   class Car {
       // ...
   }

   class Person {
       Car car;


 }
   ```

   - **Aggregation:** Aggregation represents a "has-a" relationship between objects, where one object contains or owns another object. The contained object can exist independently.

   Example:
   ```java
   class Department {
       List<Employee> employees;
   }
   ```

   - **Composition:** Composition is a stronger form of aggregation, where the lifetime of the contained object is controlled by the container object. The contained object cannot exist without the container.

   Example:
   ```java
   class Engine {
       // ...
   }

   class Car {
       Engine engine;
   }
   ```

7. **SOLID Principles:**
   - **Single Responsibility Principle (SRP):** A class should have only one reason to change.

   Example:
   ```java
   class EmailSender {
       void sendEmail() {
           // Code to send an email
       }
   }
   ```

   - **Open/Closed Principle (OCP):** Software entities (classes, modules, etc.) should be open for extension but closed for modification.

   Example:
   ```java
   abstract class Shape {
       abstract void draw();
   }

   class Circle extends Shape {
       void draw() {
           // Code to draw a circle
       }
   }
   ```

   - **Liskov Substitution Principle (LSP):** Subtypes must be substitutable for their base types without affecting the correctness of the program.

   Example:
   ```java
   class Vehicle {
       void startEngine() {
           // Code to start the engine
       }
   }

   class Car extends Vehicle {
       void startEngine() {
           // Code to start the car's engine
       }
   }
   ```

   - **Interface Segregation Principle (ISP):** Clients should not be forced to depend on interfaces they do not use. It promotes smaller, cohesive interfaces.

   Example:
   ```java
   interface Printable {
       void print();
   }

   interface Scanable {
       void scan();
   }

   class Printer implements Printable {
       void print() {
           // Code to print
       }
   }
   ```

   - **Dependency Inversion Principle (DIP):** High-level modules should not depend on low-level modules; both should depend on abstractions.

   Example:
   ```java
   interface MessageSender {
       void sendMessage();
   }

   class EmailSender implements MessageSender {
       void sendMessage() {
           // Code to send an email
       }
   }
   ```

These examples showcase the concepts and principles of object-oriented programming, providing a solid foundation for designing and building modular, reusable, and maintainable software systems.
